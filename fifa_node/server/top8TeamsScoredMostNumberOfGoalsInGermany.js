const genericForQueryManipulation = require('./index');

const query = `
SELECT
  Team,
  SUM(goals) AS goals
FROM
  (
    SELECT
      home_team_name AS Team,
      home_team_goals AS goals,
      YEAR
    FROM
      worldcupmatches
    WHERE
      home_team_goals>away_team_goals
    UNION ALL
    SELECT
      away_team_name AS Team,
      away_team_goals AS goals,
      YEAR
    FROM
      worldcupmatches
    WHERE
      home_team_goals<away_team_goals
  ) AS COMBINED
WHERE
  YEAR IN (
    SELECT
      YEAR
    FROM
      worldcups
    WHERE
      country='Germany'
  )
GROUP BY
  Team
ORDER BY
  goals DESC
LIMIT
  8`;

genericForQueryManipulation(query,'../output/top8TeamsScoredMostGoals.json');