const genericForQueryManipulation = require('./index');

const query = `
SELECT
  team,
  COUNT(team) AS winner
FROM
  (
    SELECT
      home_team_name AS team
    FROM
      worldcupmatches
    WHERE
      home_team_goals>away_team_goals
    UNION ALL
    SELECT
      away_team_name AS team
    FROM
      worldcupmatches
    WHERE
      away_team_goals>home_team_goals
  ) AS COMBINED
GROUP BY
  team`;

genericForQueryManipulation(query,'../output/matchesWonPerTeam.json');