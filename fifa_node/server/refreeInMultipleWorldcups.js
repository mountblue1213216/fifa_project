const genericForQueryManipulation = require('./index');

const query = `
SELECT
  refree,
  World_Cups
FROM
  (
    SELECT
      refree,
      COUNT(DISTINCT YEAR) AS World_Cups
    FROM
      worldcupmatches
    GROUP BY
      refree
  ) AS referee_worldcup
WHERE
  World_Cups>1
ORDER BY
  World_Cups DESC,
  refree ASC`;


genericForQueryManipulation(query,'../output/refreeInMultipleWorldCups.json');
