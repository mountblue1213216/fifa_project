const {Client} = require('pg');
const fs = require('fs').promises;

const client = new Client({
    host : 'localhost',
    user : 'postgres',
    port : 5432,
    password : 'root',
    database: 'fifa'
});

client.connect();

function genericForQueryManipulation(query,path){
    client.query(query)
      .then((res) => {
        return fs.writeFile(path,JSON.stringify(res.rows))
      })
      .then(() => {
        console.log("Data Written in JSON File");
      })
      .catch((error)=> {
        console.log(error);
      })
      .finally(() => {
        client.end()
      })
}

module.exports = genericForQueryManipulation;