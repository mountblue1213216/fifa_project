const genericForQueryManipulation = require('./index');

const query = `
SELECT
  MINUTE,
  COUNT(MINUTE) AS "Goals Scored"
FROM
  (
    SELECT
      CAST(SUBSTR (VALUE, 2, LENGTH (VALUE) - 2) AS INTEGER) AS MINUTE
    FROM
      (
        SELECT
          UNNEST (STRING_TO_ARRAY (event, ' ')) AS VALUE
        FROM
          worldcupplayers
      ) AS A
    WHERE
      VALUE LIKE 'G%'
  ) AS B
GROUP BY
  MINUTE
ORDER BY
  MINUTE`

genericForQueryManipulation(query,'../output/goalsScoredPerMinute.json');